package basicPackage;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		printWords(1, "Sheep and Wolves");
	}
	

	private static void printWords(int a, String b) {
		if(a < 1){
			System.out.println(b);
			return;
		}
		
		String temp = "";
		for(int i = 0; i < 4 + a; i++){
			temp += "~";
		}
		System.out.println(temp);
		printWords(a - 1, b);
		System.out.println(temp);
	}
	
	
}
